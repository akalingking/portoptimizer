#!/usr/bin/env python
# coding: utf-8
import os
import pandas as pd
'''Download dataset from
   https://www.kaggle.com/ashbellett/australian-historical-stock-prices'''

def load_dataset(
    dirPath: str='../data/', 
    startDate: str='2000-01-01', 
    endDate: str='2020-03-31'
) -> (str, pd.DataFrame):
    # names
    stocks = [stock.split('.')[0] for stock in sorted(os.listdir(dirPath))]
    assert(len(stocks) > 0)
    # data
    dates = pd.date_range(startDate, endDate)
    data = pd.DataFrame({'Time': dates})
    for stock in stocks:
        prices = pd.read_csv(dirPath+stock+'.csv', usecols=['Date', 'Adj Close'])
        prices['Date'] = pd.to_datetime(prices['Date'])
        prices.rename(columns={'Date': 'Time', 'Adj Close': stock}, inplace=True)
        data = pd.merge(data, prices, how='left', sort=False)

    data = data[data['Time'].dt.weekday < 5] # weekend days
    data = data.dropna(axis=0, how='all') # empty rows
    return (stocks, data)

if __name__ == '__main__':
    _, data = load_dataset(dirPath='../data/')
    print(data.head(10))
    print('..')
    print(data.tail(10))
    
