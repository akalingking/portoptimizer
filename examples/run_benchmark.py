#!/usr/bin/env python
# coding: utf-8
''' @reference
    [1] Portfolio Selection, Harry Markowitz
        https://www.jstor.org/stable/2975974?origin=crossref&seq=1
'''
import os,sys
sys.path.append('..')
from utils.datasource import load_dataset
import numpy as np
import scipy as sp
import pandas as pd
import cvxpy as cp
from utils.params import params
import matplotlib.pyplot as plt
from matplotlib import cm

def get_data():
    data_path = params['data_path']
    start_date = params['start_date']
    end_date = params['end_date']
    stocks, data = load_dataset(dirPath=data_path, startDate=start_date, endDate=end_date)
    assert(len(stocks) > 0)
    '''print(data.head(10))
    print('..')
    print(data.tail(10))'''
    return (stocks, data)
    
def run_markowitz():
    # 1. Prep dataset
    stock_names, data = get_data()
    prices = data.drop(['Time'], axis=1).tail(1).to_numpy()
    returns_df = data[(data['Time'].dt.weekday == 4) & (data['Time'] >= '2019-01-01')] \
            .drop(['Time'], axis=1) \
            .pct_change(fill_method='ffill')
    returns_df = returns_df.fillna(0.)
    returns = returns_df.to_numpy()
    
    n_samples, n_stocks = np.shape(returns_df)
    print('Number of stocks: {} n_samples: {}'.format(n_stocks, n_samples))
    
    # 2. Covariance
    mu = np.mean(returns, axis=0)
    sigma = np.cov(returns.T)
    vols = np.sqrt(sigma.diagonal())
    
    weights = cp.Variable(n_stocks)
    # risk aversion parameter
    gamma = cp.Parameter(nonneg=True)
    port_return = mu.T @ weights
    # weights^T * Sigma * weights
    port_risk = cp.quad_form(weights, sigma)
    objective = cp.Maximize(port_return - gamma * port_risk)
    constraints = [
        cp.sum(weights) == 1,
        weights >= 0,
    ]
    problem = cp.Problem(objective, constraints)
    
    # Generate portfolio risk and return data from optimizer
    n_iteration = 300
    risk_data = np.zeros(n_iteration)
    return_data = np.zeros(n_iteration)
    gamma_vals = np.logspace(-2, 4, num=n_iteration)
    for i in range(n_iteration):
        gamma.value = gamma_vals[i]
        problem.solve()
        risk_data[i] = cp.sqrt(port_risk).value
        return_data[i] = port_return.value
    
    # 3. Plot Efficient Frontier
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(risk_data, return_data, 'g-')
    markers = np.linspace(0, len(gamma_vals)-10, 5, dtype=int)
    for marker in markers:
        plt.plot(risk_data[marker], return_data[marker], 'bs')
        ax.annotate(r'$\gamma = %.2f$' % gamma_vals[marker], xy=(risk_data[marker]+.01, return_data[marker]))
    for i in range(n_stocks):
        plt.plot(cp.sqrt(sigma[i, i]).value, mu[i], 'ro')

    plt.title('Efficient Frontier')
    plt.xlabel('Standard Deviation')
    plt.ylabel('Return')
    plt.show()

if __name__ == '__main__':
    run_markowitz()

